import React, {Component} from 'react';
import { Grid } from 'semantic-ui-react';
import '../styles/style.css';

export default class Splash extends Component{
  constructor(props){
    super(props);
  }
  render(){
    return(
      <Grid>

      <Grid.Row style={{marginTop:"40%"}} only='mobile'>
        <Grid.Column width={2}></Grid.Column>
        <Grid.Column width={12} style={{textAlign:"center", fontWeight:"normal", letterSpacing:"2px",fontFamily:"Open Sans"}} ><h1>Splash Screen</h1></Grid.Column>
        <Grid.Column width={2}></Grid.Column>
      </Grid.Row>

    </Grid>
    );
  }
}
